package com.cao.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @Author: huahua
 * @Date: 2021-04-20 12:19
 */
@Data
public class User {
    private Integer uid;
    private String uname;
    private String ugh;
    private String upsw;
    private String uzw;
    private String uqx;
    private String ubmbh;
    private String inputp;
    private Date itime;
}
